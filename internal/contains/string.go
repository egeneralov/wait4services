package contains

func String(list []string, topic string) bool {
	for _, t := range list {
		if t == topic {
			return true
		}
	}
	return false
}
