package clickhouse

import (
	"context"
	"fmt"
	"net"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"go.uber.org/zap"
)

type ClickHouse struct {
	Host       string `json:"host" yaml:"host"`
	Port       int    `json:"port" yaml:"port"`
	Database   string `json:"database" yaml:"database"`
	Username   string `json:"username" yaml:"username"`
	Password   string `json:"-" yaml:"password,omitempty"`
	Timeout    int    `json:"timeout" yaml:"timeout"`
	timeout    time.Duration
	logger     *zap.Logger
	dialCount  int
	connection *driver.Conn
	ctx        context.Context
	cancel     context.CancelFunc
}

func (c *ClickHouse) Type() string {
	return "clickhouse"
}

func (c *ClickHouse) Init(log *zap.Logger) error {
	c.logger = log
	c.logger.Debug("ClickHouse service initialized", c.Fields()...)
	c.timeout = time.Duration(c.Timeout) * time.Second
	c.ctx, c.cancel = context.WithTimeout(context.Background(), c.timeout)
	return nil
}

func (c *ClickHouse) Wait() error {
	c.logger.Debug("waiting for ClickHouse service", c.Fields()...)
	start := time.Now()
	for {
		if time.Since(start) > c.timeout {
			return fmt.Errorf("timeout of %s exceeded", c.timeout)
		}
		if err := c.process(); err != nil {
			c.logger.Debug("ClickHouse service is not ready", c.Fields(zap.Error(err))...)
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	return nil
}

func (c *ClickHouse) Fields(additional ...zap.Field) []zap.Field {
	for _, field := range []zap.Field{
		zap.String("type", c.Type()),
		zap.String("host", c.Host),
		zap.Int("port", c.Port),
		zap.String("database", c.Database),
		zap.String("username", c.Username),
	} {
		additional = append(additional, field)
	}
	return additional
}

func (c *ClickHouse) process() error {
	connection, err := clickhouse.Open(&clickhouse.Options{
		Addr: []string{fmt.Sprintf("%s:%d", c.Host, c.Port)},
		Auth: clickhouse.Auth{
			Database: c.Database,
			Username: c.Username,
			Password: c.Password,
		},
		DialContext: func(ctx context.Context, addr string) (net.Conn, error) {
			c.dialCount++
			var d net.Dialer
			return d.DialContext(ctx, "tcp", addr)
		},
		Debug:    true,
		Debugf:   func(format string, v ...interface{}) { c.logger.With(c.Fields()...).Sugar().Debugf(format, v...) },
		Settings: clickhouse.Settings{"max_execution_time": c.Timeout},
		Compression: &clickhouse.Compression{
			Method: clickhouse.CompressionLZ4,
		},
		DialTimeout:      c.timeout,
		MaxOpenConns:     1,
		MaxIdleConns:     1,
		ConnMaxLifetime:  c.timeout,
		ConnOpenStrategy: clickhouse.ConnOpenInOrder,
	})
	if err != nil {
		return err
	}
	defer func() {
		if err := connection.Close(); err != nil {
			c.logger.Error("failed to close ClickHouse connection", c.Fields(zap.Error(err))...)
		}
	}()
	return connection.Ping(c.ctx)
}

func (c *ClickHouse) Close() error {
	c.logger.Debug("closing ClickHouse service", c.Fields()...)
	c.cancel()
	return nil
}
