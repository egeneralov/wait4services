package web

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"time"

	"go.uber.org/zap"
)

var httpClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	},
	Timeout: 5 * time.Second,
}

type HTTP struct {
	Url     string `json:"url" yaml:"url"`
	Method  string `json:"method" yaml:"method"`
	Status  int    `json:"status" yaml:"status"`
	Body    string `json:"-" yaml:"body,omitempty"`
	Regexp  string `json:"-" yaml:"regexp,omitempty"`
	Timeout int    `json:"timeout" yaml:"timeout"`
	timeout time.Duration
	logger  *zap.Logger
	client  *http.Client
	rxp     *regexp.Regexp
}

func (h *HTTP) Type() string {
	return "http"
}

func (h *HTTP) Fields(additional ...zap.Field) []zap.Field {
	for _, el := range []zap.Field{
		zap.String("type", h.Type()),
		zap.String("url", h.Url),
		zap.String("method", h.Method),
		zap.Int("status", h.Status),
		zap.Int("timeout", h.Timeout),
	} {
		additional = append(additional, el)
	}
	return additional
}

func (h *HTTP) Init(logger *zap.Logger) error {
	h.logger = logger
	h.logger.Debug("initializing http service", h.Fields()...)
	h.client = httpClient
	if h.Timeout < 1 {
		return fmt.Errorf("invalid timeout: %d", h.Timeout)
	}
	h.timeout = time.Duration(h.Timeout) * time.Second
	if h.Url == "" {
		return fmt.Errorf("url is required")
	}
	u, err := url.Parse(h.Url)
	if err != nil {
		return err
	}
	if u.Scheme == "" {
		return fmt.Errorf("url scheme is empty")
	}
	if u.Host == "" {
		return fmt.Errorf("url host is empty")
	}
	if u.Path == "" {
		return fmt.Errorf("url path is empty")
	}
	switch h.Method {
	case "GET", "POST", "PUT", "DELETE", "PATCH", "HEAD", "OPTIONS":
		h.logger.Debug("http method is valid", h.Fields()...)
	default:
		h.Method = "GET"
		h.logger.Warn("http method is invalid, using get as default", h.Fields()...)
	}
	if h.Status < 100 || h.Status > 599 {
		return fmt.Errorf("invalid status: %d", h.Status)
	}
	if h.Timeout > 180 {
		h.logger.Warn("timeout is too big", h.Fields()...)
	}
	if h.Body != "" && h.Regexp != "" {
		return fmt.Errorf("body and rxp are mutually exclusive")
	}
	if h.Regexp != "" {
		rxp, err := regexp.Compile(h.Regexp)
		if err != nil {
			return fmt.Errorf("invalid rxp: %s - %v", h.Regexp, err)
		}
		h.rxp = rxp
		h.logger.Debug("rxp is valid", h.Fields()...)
	}
	return nil
}

func (h *HTTP) Wait() error {
	h.logger.Debug("waiting for http service", h.Fields()...)
	now := time.Now()
	for {
		if time.Since(now) > h.timeout {
			return fmt.Errorf("timeout")
		}
		err := h.request()
		if err == nil {
			return nil
		}
		time.Sleep(1 * time.Second)
	}
}

func (h *HTTP) request() error {
	req, err := http.NewRequest(h.Method, h.Url, nil)
	if err != nil {
		return err
	}
	resp, err := h.client.Do(req)
	if err != nil {
		return fmt.Errorf("http request failed: %v", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			h.logger.Error("failed to close response body", zap.Error(err))
		}
	}()
	if resp.StatusCode != h.Status {
		return fmt.Errorf("invalid status: %d", resp.StatusCode)
	}
	if h.Body != "" || h.Regexp != "" {
		raw, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("failed to read body: %v", err)
		}
		body := string(raw)
		//h.logger.Debug("received body", zap.String("body", body))
		if h.Body != "" && body != h.Body {
			return fmt.Errorf("invalid body: %s, expected: %s", body, h.Body)
		}
		if h.Regexp != "" && !h.rxp.MatchString(body) {
			return fmt.Errorf("invalid body: %s, expected to match: %s", body, h.Regexp)
		}
	}
	return nil
}

func (h *HTTP) Close() error {
	h.logger.Debug("closing http service", h.Fields()...)
	h.client.CloseIdleConnections()
	return nil
}
