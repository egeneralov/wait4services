package mongodb

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/url"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	"gitlab.com/egeneralov/wait4services/internal/contains"
)

type MongoDB struct {
	Uri     string `json:"uri" yaml:"uri"`
	Timeout int    `json:"timeout" yaml:"timeout"`
	timeout time.Duration
	log     *zap.Logger
	cancel  context.CancelFunc
	ctx     context.Context
	start   time.Time
}

func (m *MongoDB) Type() string {
	return "mongodb"
}

func (m *MongoDB) Init(log *zap.Logger) error {
	m.log = log
	m.log.Debug("MongoDB service initialized", m.Fields()...)
	uri, err := url.Parse(m.Uri)
	if err != nil {
		return fmt.Errorf("failed to parse MongoDB URI: %w", err)
	}
	if !contains.String([]string{"mongodb", "mongodb+srv"}, uri.Scheme) || uri.Host == "" {
		return fmt.Errorf("invalid MongoDB URI: %s", m.Uri)
	}
	m.ctx, m.cancel = context.WithTimeout(context.Background(), m.timeout)
	return nil
}

func (m *MongoDB) Wait() error {
	m.start = time.Now()
	for {
		if err := m.process(); err != nil {
			m.log.Debug("failed to process MongoDB", m.Fields(zap.Error(err))...)
			if time.Since(m.start) > m.timeout {
				return fmt.Errorf("failed to wait for MongoDB: %w", err)
			}
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	return nil
}

func (m *MongoDB) Fields(additional ...zap.Field) []zap.Field {
	for _, field := range []zap.Field{
		zap.String("type", m.Type()),
		zap.String("uri", m.Uri),
		zap.Int("timeout", m.Timeout),
	} {
		additional = append(additional, field)
	}
	return additional
}

func (m *MongoDB) process() error {
	appName := "wait4services"
	opts := &options.ClientOptions{
		AppName:                &appName,
		ConnectTimeout:         nil,
		Dialer:                 nil,
		Direct:                 nil,
		MaxPoolSize:            nil,
		MinPoolSize:            nil,
		MaxConnecting:          nil,
		ReplicaSet:             nil,
		ServerSelectionTimeout: nil,
		Timeout:                nil,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	opts.ApplyURI(m.Uri)
	m.log.Sugar().Debug("connecting to MongoDB", opts)
	client, err := mongo.NewClient(opts)
	if err != nil {
		return fmt.Errorf("failed to create new MongoDB client: %w", err)
	}
	if err := client.Connect(m.ctx); err != nil {
		return fmt.Errorf("failed to connect to MongoDB: %w", err)
	}
	defer func() {
		if err := client.Disconnect(m.ctx); err != nil {
			m.log.Warn("failed to disconnect from *MongoDB", m.Fields(zap.Error(err))...)
		}
	}()
	if err := client.Ping(m.ctx, nil); err != nil {
		return fmt.Errorf("failed to ping MongoDB: %w", err)
	}
	return nil
}

func (m *MongoDB) Close() error {
	m.cancel()
	return nil
}
