package rabbitmq

import (
	"fmt"
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type RabbitMQ struct {
	Uri        string   `json:"uri" yaml:"uri"`
	Timeout    int      `json:"timeout" yaml:"timeout"`
	Queues     []string `json:"queues,omitempty" yaml:"queues,omitempty"`
	timeout    time.Duration
	connection *amqp.Connection
	log        *zap.Logger
}

func (r *RabbitMQ) Type() string {
	return "rabbitmq"
}

func (s *RabbitMQ) Init(log *zap.Logger) error {
	s.log = log
	s.timeout = time.Duration(s.Timeout) * time.Second
	s.log.Debug("RabbitMQ service initialized", s.Fields()...)
	return nil
}

func (s *RabbitMQ) Wait() error {
	start := time.Now()
	for {
		if time.Since(start) > s.timeout {
			return fmt.Errorf("timeout of %s exceeded", s.timeout)
		}
		if err := s.process(); err != nil {
			s.log.Error("failed to process RabbitMQ service", s.Fields(zap.Error(err))...)
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	return nil
}

func (s *RabbitMQ) Fields(additional ...zap.Field) []zap.Field {
	fields := []zap.Field{
		zap.String("type", s.Type()),
		zap.String("uri", s.Uri),
		zap.Strings("queues", s.Queues),
		zap.Int("timeout", s.Timeout),
	}
	return append(fields, additional...)
}

func (s *RabbitMQ) process() error {
	if s.connection == nil {
		config := amqp.Config{Properties: amqp.NewConnectionProperties()}
		hostname, _ := os.Hostname()
		config.Properties.SetClientConnectionName("wait4service on " + hostname)
		connection, err := amqp.DialConfig(s.Uri, config)
		if err != nil {
			return fmt.Errorf("failed to connect to RabbitMQ: %w", err)
		}
		s.connection = connection
	}
	for _, queue := range s.Queues {
		channel, err := s.connection.Channel()
		if err != nil {
			return fmt.Errorf("failed to open channel: %w", err)
		}
		q, err := channel.QueueInspect(queue)
		if err != nil {
			return fmt.Errorf("failed to inspect queue %s: %w", queue, err)
		}
		s.log.Debug(
			"queue inspected",
			zap.String("queue", queue),
			zap.Int("messages", q.Messages),
			zap.Int("consumers", q.Consumers),
		)
		if err := channel.Close(); err != nil {
			return fmt.Errorf("failed to close channel %v: %w", queue, err)
		}
	}
	return nil
}

func (s *RabbitMQ) Close() error {
	return s.connection.Close()
}
