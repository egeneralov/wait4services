package pgsql

import (
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"time"

	_ "github.com/lib/pq"
	"go.uber.org/zap"

	"gitlab.com/egeneralov/wait4services/internal/contains"
)

type PgSQL struct {
	Dsn     string   `json:"dsn" yaml:"dsn"`
	Timeout int      `json:"timeout" yaml:"timeout"`
	Tables  []string `json:"tables,omitempty" yaml:"tables,omitempty"`
	timeout time.Duration
	db      *sql.DB
	logger  *zap.Logger
}

func (p *PgSQL) Type() string {
	return "pgsql"
}

var (
	re = regexp.MustCompile(`.* ?host=(?P<host>[^ ]+) ?.*`)
)

func (p *PgSQL) Fields(additional ...zap.Field) []zap.Field {
	fields := []zap.Field{
		zap.String("type", p.Type()),
		zap.Int("timeout", p.Timeout),
		zap.Strings("tables", p.Tables),
	}
	match := re.FindStringSubmatch(p.Dsn)
	if match != nil && len(match) == 2 {
		fields = append(fields, zap.String("host", match[1]))
	} else {
		fields = append(fields, zap.String("dsn", p.Dsn))
	}
	return append(fields, additional...)
}

func (p *PgSQL) Init(log *zap.Logger) error {
	p.logger = log
	p.logger.Debug("initializing pgsql service", p.Fields()...)
	p.timeout = time.Duration(p.Timeout) * time.Second
	return nil
}

func (p *PgSQL) proceed() error {
	if p.db == nil {
		db, err := sql.Open("postgres", p.Dsn)
		if err != nil {
			return fmt.Errorf("failed to open database: %w", err)
		}
		p.db = db
	}
	ctx, cancel := context.WithTimeout(context.Background(), p.timeout)
	defer cancel()
	p.logger.Debug("checking database connection", p.Fields()...)
	if err := p.db.PingContext(ctx); err != nil {
		return fmt.Errorf("failed to ping database: %w", err)
	}
	if len(p.Tables) == 0 {
		p.logger.Warn("no tables to check", p.Fields()...)
		return nil
	}

	rows, err := p.db.Query("SELECT schemaname, tablename FROM pg_catalog.pg_tables;")
	if err != nil {
		return fmt.Errorf("failed to execute query: %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			p.logger.Error("failed to close rows", p.Fields(zap.Error(err))...)
		}
	}()
	var (
		result  = make(map[string][]string)
		current []string
	)
	for rows.Next() {
		var (
			schema, name string
		)
		if err := rows.Scan(&schema, &name); err != nil {
			return fmt.Errorf("failed to scan table: %w", err)
		}
		result[schema] = append(result[schema], name)
	}
	if err := rows.Err(); err != nil {
		return fmt.Errorf("failed to iterate over rows: %w", err)
	}
	for schema, tables := range result {
		for _, table := range tables {
			current = append(current, fmt.Sprintf("%s.%s", schema, table))
		}
	}
	p.logger.Debug(
		"checking tables",
		p.Fields(
			zap.Strings("expected", p.Tables),
			zap.Strings("founded", current),
		)...,
	)
	for _, table := range p.Tables {
		if !contains.String(current, table) {
			return fmt.Errorf("table %s not found", table)
		}
	}
	return nil
}

func (p *PgSQL) Wait() error {
	p.logger.Debug("waiting for pgsql service", p.Fields()...)
	start := time.Now()
	for {
		if err := p.proceed(); err != nil {
			p.logger.Debug("failed to proceed", p.Fields(zap.Error(err))...)
			if time.Since(start) > p.timeout {
				return fmt.Errorf("timeout: %w", err)
			}
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	return nil
}

func (p *PgSQL) Close() error {
	p.logger.Debug("closing pgsql service", p.Fields()...)
	return p.db.Close()
}
