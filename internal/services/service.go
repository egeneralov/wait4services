package services

import (
	"go.uber.org/zap"

	"gitlab.com/egeneralov/wait4services/internal/config"
)

type Service interface {
	Type() string
	Init(*zap.Logger) error
	Wait() error
	Close() error
	Fields(...zap.Field) []zap.Field
}

func FromConfig(cfg config.Config) []Service {
	var services []Service
	for _, el := range cfg.Tcp {
		services = append(services, &el)
	}
	for _, el := range cfg.Http {
		services = append(services, &el)
	}
	for _, el := range cfg.Kafka {
		services = append(services, &el)
	}
	for _, el := range cfg.Redis {
		services = append(services, &el)
	}
	for _, el := range cfg.PgSQL {
		services = append(services, &el)
	}
	for _, el := range cfg.RabbitMQ {
		services = append(services, &el)
	}
	for _, el := range cfg.MongoDB {
		services = append(services, &el)
	}
	for _, el := range cfg.ClickHouse {
		services = append(services, &el)
	}
	return services
}
