package kafka

import (
	"fmt"
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"go.uber.org/zap"

	"gitlab.com/egeneralov/wait4services/internal/contains"
)

type Kafka struct {
	Brokers []string `json:"brokers" yaml:"brokers"`
	Timeout int      `json:"timeout" yaml:"timeout"`
	Topics  []string `json:"topics,omitempty" yaml:"topics,omitempty"`
	timeout time.Duration
	client  *kafka.AdminClient
	logger  *zap.Logger
}

func (k *Kafka) Type() string {
	return "kafka"
}

func (k *Kafka) Fields(additional ...zap.Field) []zap.Field {
	for _, el := range []zap.Field{
		zap.String("type", k.Type()),
		zap.Strings("brokers", k.Brokers),
		zap.Int("timeout", k.Timeout),
		zap.Strings("topics", k.Topics),
	} {
		additional = append(additional, el)
	}
	return additional
}

func (k *Kafka) Init(log *zap.Logger) error {
	k.logger = log
	k.logger.Debug("initializing kafka service", k.Fields()...)
	if len(k.Brokers) == 0 {
		return fmt.Errorf("brokers is empty")
	}
	if k.Timeout < 1 {
		return fmt.Errorf("timeout is less than 1")
	}
	if k.Timeout > 180 {
		k.logger.Warn("timeout is too big", k.Fields()...)
	}
	k.timeout = time.Duration(k.Timeout) * time.Second
	client, err := kafka.NewAdminClient(&kafka.ConfigMap{
		"bootstrap.servers": strings.Join(k.Brokers, ","),
	})
	if err != nil {
		return err
	}
	k.client = client
	return nil
}

func (k *Kafka) Wait() error {
	k.logger.Debug("waiting for kafka service", k.Fields()...)
	start := time.Now()
	for {
		if time.Since(start) > k.timeout {
			return fmt.Errorf("timeout")
		}
		if err := k.process(); err != nil {
			k.logger.Warn("failed to process kafka service", k.Fields(zap.Error(err))...)
			time.Sleep(1 * time.Second)
			continue
		}
		return nil
	}
}

func (k *Kafka) process() error {
	if len(k.Topics) > 0 {
		list, err := k.listTopics()
		if err != nil {
			return fmt.Errorf("failed to list topics: %w", err)
		}
		if len(k.Topics) == 0 {
			k.logger.Warn("no topics to check", k.Fields(zap.Error(err))...)
			return nil
		}
		k.logger.Debug("topics", k.Fields(zap.Strings("topics", list))...)
		for _, topic := range k.Topics {
			if !contains.String(list, topic) {
				return fmt.Errorf("topic %s not found", topic)
			}
		}
	}
	return nil
}

func (k *Kafka) listTopics() ([]string, error) {
	meta, err := k.client.GetMetadata(nil, true, 1000)
	if err != nil {
		return nil, fmt.Errorf("failed to get metadata: %w", err)
	}
	var topics []string
	for _, topic := range meta.Topics {
		if len(topic.Partitions) == 0 {
			return nil, fmt.Errorf("no partitions for topic %s", topic.Topic)
		}
		for _, partition := range topic.Partitions {
			if partition.Error.Code() != kafka.ErrNoError {
				return nil, fmt.Errorf("partition %d for topic %s has error %s", partition.ID, topic.Topic, partition.Error.Error())
			}
		}
		topics = append(topics, topic.Topic)
	}
	return topics, nil
}

func (k *Kafka) Close() error {
	k.logger.Debug("closing kafka service", k.Fields()...)
	k.client.Close()
	return nil
}
