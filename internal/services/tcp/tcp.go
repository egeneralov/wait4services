package tcp

import (
	"fmt"
	"net"
	"time"

	"go.uber.org/zap"
)

type Tcp struct {
	Network string `yaml:"network" json:"network"` // ["tcp", "tcp4", "tcp6", "udp", "udp4", "udp6", "ip", "ip4", "ip6", "unix", "unixgram", "unixpacket"]
	Address string `yaml:"address" json:"address"`
	Timeout int    `yaml:"timeout" json:"timeout"`
	logger  *zap.Logger
}

func (t *Tcp) Type() string {
	return "tcp"
}

func (t *Tcp) Init(log *zap.Logger) error {
	t.logger = log
	t.logger.Debug("Initializing TCP service")
	return nil
}

func (t *Tcp) Wait() error {
	t.logger.Debug("Waiting for TCP service", t.Fields()...)
	start := time.Now()
	for {
		if time.Since(start) > time.Duration(t.Timeout)*time.Second {
			return fmt.Errorf("timeout")
		}
		if err := t.check(); err != nil {
			t.logger.Debug("TCP service is not ready", t.Fields(zap.Error(err))...)
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	return nil
}

func (t *Tcp) Close() error {
	t.logger.Debug("Closing TCP service")
	return nil
}

func (t *Tcp) check() error {
	conn, err := net.DialTimeout(t.Network, t.Address, time.Duration(t.Timeout)*time.Second)
	if err != nil {
		return fmt.Errorf("failed to dial: %w", err)
	}
	if err := conn.Close(); err != nil {
		return fmt.Errorf("failed to close connection: %w", err)
	}
	return nil
}

func (t *Tcp) Fields(additional ...zap.Field) []zap.Field {
	for _, el := range []zap.Field{
		zap.String("type", t.Type()),
		zap.String("network", t.Network),
		zap.String("address", t.Address),
		zap.Int("timeout", t.Timeout),
	} {
		additional = append(additional, el)
	}
	return additional
}
