package redis

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
)

type Redis struct {
	Address  string `json:"address" yaml:"address"`
	Password string `json:"password,omitempty" yaml:"password,omitempty"`
	Db       int    `json:"db" yaml:"db"`
	Timeout  int    `json:"timeout" yaml:"timeout"`
	timeout  time.Duration
	client   *redis.Client
	logger   *zap.Logger
}

func (r *Redis) Type() string {
	return "redis"
}

func (r *Redis) Fields(additional ...zap.Field) []zap.Field {
	for _, el := range []zap.Field{
		zap.String("type", r.Type()),
		zap.String("addr", r.Address),
		zap.String("password", r.Password),
		zap.Int("db", r.Db),
		zap.Int("timeout", r.Timeout),
	} {
		additional = append(additional, el)
	}
	return additional
}

func (r *Redis) Init(log *zap.Logger) error {
	r.logger = log
	r.logger.Debug("initializing redis service", r.Fields()...)
	if r.Address == "" {
		return fmt.Errorf("address is empty")
	}
	if r.Timeout < 1 {
		return fmt.Errorf("timeout is less than 1")
	}
	if r.Timeout > 180 {
		r.logger.Warn("timeout is too big", r.Fields()...)
	}
	r.timeout = time.Duration(r.Timeout) * time.Second
	r.client = redis.NewClient(&redis.Options{
		Addr:     r.Address,
		Password: r.Password,
		DB:       r.Db,
	})
	return nil
}

func (r *Redis) Wait() error {
	r.logger.Debug("waiting for redis service", r.Fields()...)
	start := time.Now()
	for {
		if time.Since(start) > r.timeout {
			return fmt.Errorf("timeout")
		}
		if err := r.proceed(); err != nil {
			r.logger.Warn("failed to process redis service", r.Fields(zap.Error(err))...)
			time.Sleep(1 * time.Second)
			continue
		}
		return nil
	}
}

func (r *Redis) proceed() error {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)
	defer cancel()
	if err := r.client.Ping(ctx).Err(); err != nil {
		return fmt.Errorf("failed to ping redis: %w", err)
	}
	return nil
}

func (r *Redis) Close() error {
	r.logger.Debug("closing redis service", r.Fields()...)
	return r.client.Close()
}
