package config

import (
	"os"

	"gopkg.in/yaml.v2"

	"gitlab.com/egeneralov/wait4services/internal/services/clickhouse"
	"gitlab.com/egeneralov/wait4services/internal/services/kafka"
	"gitlab.com/egeneralov/wait4services/internal/services/mongodb"
	"gitlab.com/egeneralov/wait4services/internal/services/pgsql"
	"gitlab.com/egeneralov/wait4services/internal/services/rabbitmq"
	"gitlab.com/egeneralov/wait4services/internal/services/redis"
	"gitlab.com/egeneralov/wait4services/internal/services/tcp"
	"gitlab.com/egeneralov/wait4services/internal/services/web"
)

type Config struct {
	Tcp        []tcp.Tcp               `yaml:"tcp" json:"tcp"`
	Http       []web.HTTP              `yaml:"http" json:"http"`
	Kafka      []kafka.Kafka           `yaml:"kafka" json:"kafka"`
	Redis      []redis.Redis           `yaml:"redis" json:"redis"`
	PgSQL      []pgsql.PgSQL           `yaml:"pgsql" json:"pgsql"`
	RabbitMQ   []rabbitmq.RabbitMQ     `yaml:"rabbitmq" json:"rabbitmq"`
	MongoDB    []mongodb.MongoDB       `yaml:"mongodb" json:"mongodb"`
	ClickHouse []clickhouse.ClickHouse `yaml:"clickhouse" json:"clickhouse"`
}

func FromFile(path string) (Config, error) {
	cfg := Config{}
	bytes, err := os.ReadFile(path)
	if err != nil {
		return cfg, err
	}
	if err := yaml.Unmarshal(bytes, &cfg); err != nil {
		return cfg, err
	}
	return cfg, nil
}
