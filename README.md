# wait for services

Init container to wait for your services to be ready. See [sample configuration file](wait4services.yaml) for more
details.

## services

- [x] tcp endpoint
- [x] http endpoint
- [ ] grpc endpoint
- [x] pgsql
- [ ] stolon
- [x] redis
- [ ] keydb
- [x] rabbitmq
- [x] kafka
- [x] mongodb
- [x] clickhouse
- [ ] elastic
- [ ] mysql
