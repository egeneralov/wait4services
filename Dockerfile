FROM golang:1.18.3
RUN \
  apt-get update -q && \
  apt-get install -yq librdkafka-dev musl-tools && \
  apt-get clean -yq
WORKDIR /go/src/gitlab.com/egeneralov/wait4services
ADD go.mod go.sum ./
RUN go mod download -x
COPY . .
RUN go build -v -o /go/bin/wait4services --ldflags '-linkmode external -extldflags "-static"' cmd/wait4services/main.go

FROM debian:bullseye-slim
COPY --from=0 /go/bin/wait4services /bin/wait4services
#COPY --from=0 /go/src/gitlab.com/egeneralov/wait4services/wait4services.yaml /etc/wait4services.yaml
ENTRYPOINT ["/bin/wait4services"]