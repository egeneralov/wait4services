package main

import (
	"fmt"
	"sync"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/egeneralov/wait4services/internal/config"
	"gitlab.com/egeneralov/wait4services/internal/services"
)

var (
	cfg         = config.Config{}
	logger      *zap.Logger
	wg          = sync.WaitGroup{}
	total       = 0
	processed   = 0
	processedMx = sync.Mutex{}
	proceed     []services.Service
)

func init() {
	zl, err := zap.Config{
		Level:       zap.NewAtomicLevelAt(zapcore.DebugLevel),
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding: "json",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			FunctionKey:    zapcore.OmitKey,
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.EpochTimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
	}.Build()
	if err != nil {
		panic(fmt.Errorf("failed to init logger: %w", err))
	}
	logger = zl.With(zap.String("app", "wait4services"))
	logger.Info("starting")
	cfg, err = config.FromFile("wait4services.yaml")
	if err != nil {
		logger.Fatal("failed to load config", zap.Error(err))
	}
	proceed = services.FromConfig(cfg)
	total = len(proceed)
	wg.Add(total)
	logger.Info("config loaded", zap.Any("config", proceed), zap.Int("total", total))
}

func main() {
	go func() {
		for {
			time.Sleep(5 * time.Second)
			logger.Info("progress", zap.Int("processed", processed), zap.Int("total", total))
		}
	}()
	for _, el := range proceed {
		go func(el services.Service) {
			logger.Debug("starting service waiter", el.Fields()...)
			defer logger.Debug("exited service waiter", el.Fields()...)
			if err := el.Init(logger); err != nil {
				logger.Fatal("failed to init service", el.Fields(zap.Error(err))...)
			}
			if err := el.Wait(); err != nil {
				logger.Fatal("failed to wait service", el.Fields(zap.Error(err))...)
			}
			logger.Info("service is ready", el.Fields()...)
			if err := el.Close(); err != nil {
				logger.Error("failed to close service", el.Fields(zap.Error(err))...)
			}
			incrementProcessed()
			wg.Done()
		}(el)
	}
	wg.Wait()
	logger.Info("all services are ready", zap.Int("total", total), zap.Int("processed", processed))
}

func incrementProcessed() {
	logger.Debug("incrementing processed counter")
	processedMx.Lock()
	defer processedMx.Unlock()
	processed++
	logger.Info("service is ready", zap.Int("processed", processed), zap.Int("total", total))
}
